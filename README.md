
### Overview:

> Stop and Start an environment which runs services dependent on each other.

Specialized Ansible Play to sequentially stop and/or start a service dependent multi-host environment.  
Based on *sorted* order of hosts in the inventory.  
"Stop" is the default order.  
NOTE! THIS IS A DUMMY PLAY!  

### Usage:
Run like: $ `ansible-playbook -i environments/<env> main.yml -t <tagname>`  
Tags must be used for this playbook.  
To stop, use tagname "stop".  
To start, use tagname "start".  
 
### Stop and Start:

- STOP:  
First the overlying application layer is stopped.  
These are systemd based services.  
Secondly the service level layer is stopped.  
These services are a mix of systemd services and shell commands.  
Lastly, lingering or hanging processes are killed.  

- START:  
First temporary files are deleted before startup.  
Then the service level layer is started.    
Lastly the overlying application layer is started.  


  
---
### Sequential order:  

| -----------Stop-----------|-----------Start------------ 
| :-----------------:|:-----:|

| Server type   | Sequence  | Server type   | Sequence
|---------------|:-------:  |---------------|:-------:
| Web 02        | 1         | Controller    | 1
| Web 01        | 2         | Backend 01    | 2
| Mid 02        | 3         | Backend 02    | 3
| Mid 01        | 4         | Mid 01        | 4
| Backend 02    | 5         | Mid 02        | 5
| Backend 01    | 6         | Web 01        | 6
| Controller    | 7         | Web 02        | 7	
---
### Ansible techniques used:  
		
---
In Ansible, sequential handling of target hosts are achieved by applying the keyword 'serial'.  
Setting 'serial' to 1 indicate that the entire play is run all the way through against the first host in the inventory, then the next, then the next after that, and so forth.  
This can be seen in the "main.yml" file.  

Reverse order of the inventory, is handled by the file "divide.yml".  
Here, the list of target hosts are reversed.  


### The 'ansible.cfg' file:
The "./ansible.cfg" file has been modified to include:  
`display_skipped_hosts = False`  
This way, screen output will be less clotted.   

### Mitogen:
The Mitogen-plugin has been enabled to radically speed up execution of the "web_logclean" role, which extensively uses the ‘find’ and ‘file’ modules.  
Additions the the "./ansible.cfg" file are as follows:  
`strategy_plugins = /usr/share/ansible/plugins/strategy/mitogen-0.2.9/ansible_mitogen/plugins/strategy`  
`strategy = mitogen_linear`  
Comment out if you don't use it.  
### Ansible connection to targets:
NOTE! How you connect to target hosts will vary from organization to organization, and the following may need to change in order to suit your own needs.  
  
Using Ansible CLI, normal playbook execution - for this play - is done with an admin-user.  
For connection to the target hosts, however, in some cases ssh is established directly to the 'root' user and is set on a per task basis.  
This is necessary in order take advantage of script files in /etc/profile.d/ and have these scripts set environment variables needed.  
Some task files will, for this reason, include "become_user: root" (e.g. in case of stopping/starting services), and other task files uses "remote_user: root" - typically when executing shell commands.  

 
